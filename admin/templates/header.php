<?php if(!defined('IN_SITE')) exit('Access Denied'); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Page-Enter" Content="blendTrans(Duration=0.3)"> 
<meta http-equiv="Page-Exit" Content="blendTrans(Duration=0.3)"> 
<title>管理员控制面板</title>
<link rel="stylesheet" href="<?php echo $_TEMPLATESDIR;?>css.css" />
<link rel="stylesheet" href="<?php echo $_TEMPLATESDIR;?>css/wui.css" />
<link type="text/css" rel="stylesheet" href="script/Date_Time/skin/WdatePicker.css">

<!--<script src="script/prototype.js" type="text/javascript"></script>
--><script src="script/class.js" type="text/javascript"></script>
<script src="script/common.js" type="text/javascript"></script>
<script src="script/admin.js" type="text/javascript"></script>
<script src="script/check.js" type="text/javascript"></script>
<script src="script/Date_Time/WdatePicker.js" type="text/javascript"></script>

<script src="script/jquery-1.8.0.min.js" type="text/javascript"></script>
</head>
<body>