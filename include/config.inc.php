<?php
//配置文件

//数据库服务器地址
$db_host	= '127.0.0.1';

//数据库服务器用户名
$db_user	= 'root';

//数据库密码
$db_pass	= '';

//数据库名称
$db_name	= 'durl';

//数据表前缀
$tablepre	= 'durl_';

//程序调试模式,设定为true为开启，false关闭
$enable_debugmode	= true;

?>